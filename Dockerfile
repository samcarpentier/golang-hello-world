FROM golang:1.11.2-alpine AS compile

RUN mkdir -p /go/src/golang-hello-world
WORKDIR /go/src/golang-hello-world

COPY main.go main.go

RUN apk add --update \
  git \
  build-base

RUN go get .
RUN go install .

FROM alpine:3.8
COPY --from=compile /go/bin/golang-hello-world  /bin

ARG HTTP_PROXY
ARG HTTPS_PROXY

RUN apk add --update curl

EXPOSE 8000
HEALTHCHECK CMD curl -f -k http://localhost:8000/health || exit 1
CMD ["/bin/golang-hello-world"]
