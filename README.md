# golang-hello-world

Simple HTTP server that returns the hostname of the host it's running on. Ideal to test load-balancing configurations.
Alternatively it also serves to return the region of the cluster it is running in.

## Variables

| Name | Default Value | Description |
| :--- | :---: | :--- |
| HOST_HOSTNAME | `myself` | The hostname to return on `GET @ /`. Useful to test load-balacing use-cases |
| REGION |  | The region to return on `GET @ /region`. Useful to get the region of the cluster it is running in |

## Query Params

| Name | Default Value | Description |
| :--- | :---: | :--- |
| delay | 0 | Latency in milliseconds to introduce to each request |
| random | false | Randomize latency between 0 and the value of `delay` (true/false) |

## Examples

```
$ curl 'localhost:8000'
{ "message": "Hello from 'myself'" }
```

```
$ curl 'localhost:8000?delay=1000'
{ "message": "Hello from 'myself' (delayed by 1000ms)" }
```

```
$ curl 'localhost:8000?delay=1000&random=true'
{ "message": "Hello from 'myself' (delayed by 762ms)" }
```
```
$ curl 'localhost/region:8000'
NANE1
```
