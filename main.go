package main

import (
	"net/http"
	"os"
	"fmt"
	"strconv"
	"time"
	"math/rand"

)

func sayHello(w http.ResponseWriter, r *http.Request) {
	var delayInMs, _ = strconv.Atoi(r.URL.Query().Get("delay"))
	var isRandom, _ = strconv.ParseBool(r.URL.Query().Get("random"))
	if isRandom {
		delayInMs = rand.Intn(delayInMs)
	}

	time.Sleep(time.Millisecond * time.Duration(int64(delayInMs)))

	hostname := os.Getenv("HOST_HOSTNAME")
	if hostname == "" {
		hostname = "myself"
	}

	if delayInMs > 0 {
		w.Write([]byte(fmt.Sprintf("{ \"message\": \"Hello from '%s' (delayed by %dms)\" }", hostname, delayInMs)))
	} else {
	w.Write([]byte(fmt.Sprintf("{ \"message\": \"Hello from '%s'\" }", hostname)))
	}
}

func returnRegion(w http.ResponseWriter, r *http.Request) {
	region := os.Getenv("REGION")

	w.Write([]byte(region))
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("{ \"status\": \"alive\" }")))
}

func main() {
	if _, isRegionVariableDefined := os.LookupEnv("REGION"); !isRegionVariableDefined {
		panic("Mandatory environment variable REGION is not defined. Exiting.")
	}
	
	rand.Seed(time.Now().UnixNano())
	
	http.HandleFunc("/", sayHello)
	http.HandleFunc("/region", returnRegion)
	http.HandleFunc("/health", healthcheck)
	
	if err := http.ListenAndServe(":8000", nil); err != nil {
		panic(err)
	}
}
